using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace LV6Z1
{
    interface IAbstractIterator
    {
        Note First();
        Note Next();
        bool IsDone { get; }
        Note Current { get; }
    }
}