using System;
using System.Collections.Generic;
using System.Text;

namespace LV6Z1
{
    interface IAbstractCollection
    {
        IAbstractIterator GetIterator();
    }
}