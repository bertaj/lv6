using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threding.Tasks;

namespace LV6Z1
{
    class Program
    {
        static void Main(string[] args)
        {
            Notebook notebook = new Notebook();

            notebook.AddNote(new Note("SomeTitle1", "SomeNote1"));
            
            notebook.AddNote(new Note("SomeTitle2", "SomeNote2"));
            
            notebook.AddNote(new Note("SomeTitle3", "SomeNote3"));

            Iterator iterator = notebook.GetIterator();

            while (iterator.IsDone != true)
            {
                iterator.Current.Show();
                iterator.Next();
            }
        }
    }
}
